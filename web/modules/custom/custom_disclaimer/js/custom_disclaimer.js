/**
 * @file
 * Disclaimer popup.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  console.log('custom_disclaimer.js');

  Drupal.behaviors.custom_disclaimer = {
    attach: function (context, settings) {

      // Go trough all disclaimer block instances on this page.
      $.each(drupalSettings.disclaimer, function (index, value) {

        console.log(index);
        console.log(value);

        // Skip popup in case cookie says user already agreed.
        if ($.cookie(index) !== '1') {
          // User did not agreed yet. Show popup.
          $('<div>', context).dialog({
            closeOnEscape: false,
            open: function (event, ui) {
              $('.ui-dialog-titlebar-close', ui.dialog | ui).hide();
              $('.ui-dialog-content').html(value.text);
            },
            resizable: false,
            height: 'auto',
            width: '40%',
            modal: true,
            buttons: {
              'accept': {
                text: 'Accept',
                click: function () {
                  $(this).dialog('close');
                  var expire = new Date(new Date().getTime() + 31556926);
                  $.cookie(index, '1', {expires: expire});
                }
              },
              'decline': {
                text: 'Decline',
                click: function () {
                  $(this).dialog('close');
                  window.location.href = '/';
                }
              }
            }
          });
        }


      });
    }
  };

})(jQuery, Drupal, drupalSettings);

